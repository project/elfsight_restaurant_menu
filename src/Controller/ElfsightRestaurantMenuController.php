<?php

namespace Drupal\elfsight_restaurant_menu\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightRestaurantMenuController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/restaurant-menu/?utm_source=portals&utm_medium=drupal&utm_campaign=restaurant-menu&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
